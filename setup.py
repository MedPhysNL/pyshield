#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 26 12:56:47 2017

@author: HeyDude
"""

from setuptools import setup, find_packages


VERSION = 0.2
DESCRIPTION = ('Pyshield is a package for nuclear medicine departments to'
               'calculate the necessary amount of shielding for'
               'radionuclide labs, pet and spect scanners.')

NAME = 'pyshield'



REQUIRES = ['pandas', 
            'natsort', 
            'numpy',
            'scipy', 
            'pyyaml', 
            'xlsxwriter', 
            'matplotlib', 
            'openpyxl',
            'pathos',
#            'shutil',
            'xlrd',
            'argparse',
            'openpyxl']



setup(name=NAME,
      version=VERSION,
      description=DESCRIPTION,
      classifiers=[
        'Development Status :: 3 - Alpha',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3.6',
        'Topic :: Scientific/Engineering :: Medical Science Apps.',
        'Intended Audience :: Science/Research',
        'Natural Language :: English'
      ],
      package_data={"": ['*.xls', '*.yml']},
      keywords='shielding nuclear medicine, isotopes, pet, spect',
      # url='https://github.com/heydude1337/pyshield',
      author='Marcel Segbers',
      author_email='msegbers@gmail.com',
      license='MIT',
      packages=find_packages(),
      install_requires=REQUIRES,
      include_package_data=True,
      zip_safe=False)