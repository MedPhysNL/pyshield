
PHYSICS_CONFIG =                'physics.yml'

ISOTOPES =                      'isotopes'
#items
H10 =                           'h(10) [uSv/h per MBq/m^2]' # not used (!)
DECAY_CONSTANT =                'labda [s^-1]'
ABUNDANCE =                     'Abundance'
BUILDUP_FACTORS =               'buildup_factors'
MATERIALS =                     'materials'
DENSITY =                       'Density [g/cm^3]'
ATTENUATION =                   'attenuation'
MFP =                           'mfp'
ABUNDANCE =                     'Abundance'
ENERGY_keV =                    'Energy [keV]'
ENERGY_MeV =                    'Energy [MeV]'
MASS_ATTENUATION =              'mu/p [cm^2/g]'
