import yaml
import os
import pandas as pd
from pyshield.constants import PHYSICS_CONFIG
folder = os.path.dirname(__file__)

def load(item):
    full_item = os.path.join(folder, item)
    
    if os.path.splitext(full_item)[1] == '.yml':
        with open(full_item) as file:
            contents = yaml.safe_load(file)
    elif os.path.splitext(full_item)[1] == '.xls':
        contents = pd.read_excel(full_item, sheet_name=None)
    else:
        raise ValueError(item)
        
    
    return contents
                    
            
            



physics_file = os.path.join(folder, PHYSICS_CONFIG)

with open(physics_file) as file:
    PHYSICS = yaml.safe_load(file)
    
for key, item in PHYSICS.items():
    PHYSICS[key] = load(item)
    

    
    
